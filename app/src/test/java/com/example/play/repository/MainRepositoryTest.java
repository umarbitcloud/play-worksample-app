package com.example.play.repository;

import com.example.play.loader.PlayDetailLoader;
import com.example.play.loader.PlayLoader;
import com.example.play.util.ConnectivityChecker;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import static org.mockito.Mockito.when;

public class MainRepositoryTest {

    @Mock
    private PlayLoader mMockPlayLoader;

    @Mock
    private PlayDetailLoader mMockPlayDetailLoader;

    @Mock
    ConnectivityChecker mMockConnectivityChecker;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private MainRepository mRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mRepository = new MainRepository(mMockPlayLoader, mMockPlayDetailLoader, mMockConnectivityChecker);
    }

    @Test
    public void verifyLoaderIsInvokedOnGetPlaySectionList() {
        when(mMockConnectivityChecker.isConnected()).thenReturn(true);

        mRepository.getPlaySectionList();

        Mockito.verify(mMockPlayLoader).requestData(null);
    }

    @Test
    public void verifyClearObserverIsInvokedOnClearPlayLoader() {
        mRepository.clearPlaySectionListLoader();

        Mockito.verify(mMockPlayLoader).clearObserver();
    }

    @Test
    public void verifyLoaderIsInvokedWithProperParamsOnGetPLaySectionDetail() {
        when(mMockConnectivityChecker.isConnected()).thenReturn(true);
        mRepository.getPlaySectionDetail("data", "section_id");

        Mockito.verify(mMockPlayDetailLoader).requestData(ArgumentMatchers.eq("data"));
    }

    @Test
    public void verifyClearObserverIsInvokedOnClearPlayDetailLoader() {
        mRepository.clearPlaySectionDetailLoader();

        Mockito.verify(mMockPlayDetailLoader).clearObserver();
    }

    @Test
    public void verifyGetServerPlaySelectionListFromDB() {
        when(mMockConnectivityChecker.isConnected()).thenReturn(false);
        mRepository.getPlaySectionList();

        Mockito.verify(mMockPlayLoader).getServerPlaySectionListFromDB();
    }

    @Test
    public void verifyLoaderIsInvokedWithProperParamsOnGetPlaySectionDetailFromDB() {
        when(mMockConnectivityChecker.isConnected()).thenReturn(false);
        mRepository.getPlaySectionDetail("data", "section_id");

        Mockito.verify(mMockPlayDetailLoader).getServerPlaySelectionFromDB(ArgumentMatchers.eq("section_id"));
    }
}
