package com.example.play.view;

import com.example.play.data.model.server.ServerPlaySelection;
import com.example.play.event.play.PlayErrorEvent;
import com.example.play.event.play.PlaySuccessEvent;
import com.example.play.repository.MainRepository;
import com.example.play.ui.list.ListViewModel;
import com.example.play.util.ConnectivityChecker;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class ListViewModelTest {

    private ListViewModel mViewModel;

    private final PlaySuccessEvent PLAY_SUCCESS_EVENT = PlaySuccessEvent.playsEvent(new ArrayList<ServerPlaySelection>() {{
        add(getServerPlayListResponse());
    }});

    private final PlayErrorEvent PLAY_ERROR_EVENT = new PlayErrorEvent();

    @Mock
    MainRepository mMockRepository;

    @Mock
    EventBus mMockEventBus;

    @Mock
    ConnectivityChecker mConnectivityChecker;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verifyRegisterEventBusHappensOnCreatingViewModel() {
        mViewModel = new ListViewModel(mMockRepository, mMockEventBus);
        Mockito.verify(mMockEventBus).register(Matchers.any());
    }

    @Test
    public void verifyThatTheDataIsLoadedFromDbOnDeviceIsInOffline() {
        Mockito.when(mConnectivityChecker.isConnected()).thenReturn(false);
        mViewModel = new ListViewModel(mMockRepository, mMockEventBus);

        Mockito.verify(mMockRepository).getPlaySectionList();
    }

    @Test
    public void verifyThatTheDataIsLoadedFromApiOnDeviceIsInOnline() {
        Mockito.when(mConnectivityChecker.isConnected()).thenReturn(true);
        mViewModel = new ListViewModel(mMockRepository, mMockEventBus);

        Mockito.verify(mMockRepository).getPlaySectionList();
    }

    @Test
    public void verifyViewObserversSetOnSuccessEvent() {
        mViewModel = new ListViewModel(mMockRepository, mMockEventBus);

        mViewModel.onEvent(PLAY_SUCCESS_EVENT);

        assertThat(mViewModel.getError().getValue()).isEqualTo(false);
        assertThat(mViewModel.getLoading().getValue()).isEqualTo(false);
        assertThat(mViewModel.getPlays().getValue()).isEqualTo(PLAY_SUCCESS_EVENT.getPlays());
    }

    @Test
    public void verifyViewErrorObserversSetOnErrorEvent() {
        mViewModel = new ListViewModel(mMockRepository, mMockEventBus);

        mViewModel.onEvent(PLAY_ERROR_EVENT);

        assertThat(mViewModel.getError().getValue()).isEqualTo(true);
        assertThat(mViewModel.getLoading().getValue()).isEqualTo(false);
    }

    private ServerPlaySelection getServerPlayListResponse() {
        ServerPlaySelection serverPlaySelection = new ServerPlaySelection();
        serverPlaySelection.setId("id");
        serverPlaySelection.setLink("link");
        serverPlaySelection.setTitle("title");
        serverPlaySelection.setType("type");
        return serverPlaySelection;
    }
}