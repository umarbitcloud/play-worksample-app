package com.example.play.extra;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class RecyclerViewMatcher {
    private final int mRecyclerViewId;

    public RecyclerViewMatcher(int recyclerViewId) {
        mRecyclerViewId = recyclerViewId;
    }

    public Matcher<View> atPositionOnView(final int position, final int targetViewId) {

        return new TypeSafeMatcher<View>() {
            Resources mResources = null;
            View mChildView;

            public void describeTo(Description description) {
                String idDescription = Integer.toString(mRecyclerViewId);

                if (mResources != null) {
                    try {
                        idDescription = this.mResources.getResourceName(mRecyclerViewId);
                    } catch (Resources.NotFoundException var4) {
                        idDescription = String.format("%s (resource name not found)", new Object[]{Integer.valueOf(mRecyclerViewId)});
                    }
                }

                description.appendText("with id: " + idDescription);
            }

            public boolean matchesSafely(View view) {

                mResources = view.getResources();

                if (mChildView == null) {
                    RecyclerView recyclerView = (RecyclerView) view.getRootView().findViewById(mRecyclerViewId);
                    if (recyclerView != null && recyclerView.getId() == mRecyclerViewId) {
                        mChildView = recyclerView.findViewHolderForAdapterPosition(position).itemView;
                    } else {
                        return false;
                    }
                }

                if (targetViewId == -1) {
                    return view == mChildView;
                } else {
                    View targetView = mChildView.findViewById(targetViewId);
                    return view == targetView;
                }
            }
        };
    }
}
