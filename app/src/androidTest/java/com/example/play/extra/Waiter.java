package com.example.play.extra;

import android.support.test.espresso.matcher.ViewMatchers;

import static android.support.test.espresso.Espresso.onView;

public class Waiter {

    private static void waitFor(long millis) {
        onView(ViewMatchers.isRoot()).perform(ExtraViewActions.waitFor(millis));
    }

    public static void waitForTwoSecond() {
        waitFor(2000);
    }
}
