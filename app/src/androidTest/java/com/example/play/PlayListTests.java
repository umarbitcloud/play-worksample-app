package com.example.play;


import com.example.play.extra.RecyclerViewMatcher;
import com.example.play.extra.Waiter;
import com.example.play.fakeserver.MockServer;
import com.example.play.rule.PlayRule;
import com.example.play.ui.main.MainActivity;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

public class PlayListTests {

    private final String PLAY_URL_ENDPOINT
            = "androiddash-se";

    @Rule
    public PlayRule mPlayRule = new PlayRule<>(MainActivity.class, false);

    @Before
    public void setUp() {
        MockServer.setup();
    }

    @Test
    public void showPlayListWithContent() {
        MockServer.enqueueSuccessResponse(PLAY_URL_ENDPOINT, "play/play_list_success.json");

        mPlayRule.launchActivity(null);
        onView(allOf(withId(R.id.play_selection_list), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

        Waiter.waitForTwoSecond();

        intended(allOf(hasComponent(MainActivity.class.getName())));

    }

    @Test
    public void showPlayListAndVerifyRowData() {
        MockServer.enqueueSuccessResponse(PLAY_URL_ENDPOINT, "play/play_list_success.json");

        mPlayRule.launchActivity(null);

        onView(allOf(withId(R.id.play_selection_list), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

        Waiter.waitForTwoSecond();

        verifyRowData(0, "Serier");
        verifyRowData(1, "Film");
        verifyRowData(3, "Sport");

    }

    @Ignore
    @Test
    public void showsPlayEmptyMessage() {
        //MockServer.enqueueSuccessResponse(PLAY_URL_ENDPOINT, "responses/play/play_list_empty.json");
        // Need to handle
    }

    @Ignore
    @Test
    public void showPlayError() {
        //MockServer.enqueueFailedResponse(PLAY_URL_ENDPOINT, 500, "api_failure.json", 100);
        // Need to handle
    }


    private void verifyRowData(int position, String playTitle) {
        rowData(position, R.id.play_title).check(matches(withText(playTitle)));
    }

    private ViewInteraction rowData(int position, int childViewId) {
        return onView(new RecyclerViewMatcher(R.id.play_selection_list).atPositionOnView(position, childViewId));
    }

}
