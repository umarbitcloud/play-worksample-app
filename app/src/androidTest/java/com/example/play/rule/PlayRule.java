package com.example.play.rule;


import com.example.play.fakeserver.MockServer;
import com.example.play.util.ActivityLauncher;
import com.example.play.util.ThreadPoolIdlingResource;

import org.junit.rules.ExternalResource;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.intent.Intents;

import java.util.concurrent.ThreadPoolExecutor;

import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static com.example.play.di.module.ApplicationModule.okHttpClient;
import static org.hamcrest.CoreMatchers.not;

public class PlayRule<T extends Activity> extends ExternalResource {

    private boolean mLaunchActivity;
    private ActivityLauncher<T> mActivityLauncher;

    public PlayRule(Class<T> activityClass) {
        this(activityClass, true);
    }

    public PlayRule(Class<T> activityClass, final boolean launchActivity) {
        mLaunchActivity = launchActivity;
        mActivityLauncher = new ActivityLauncher<>(activityClass);
    }

    @Override
    protected void before() throws Throwable {
        super.before();
        Intents.init();
        MockServer.setup();
        Instrumentation.ActivityResult intentResult = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        IdlingRegistry.getInstance().register(new ThreadPoolIdlingResource(
                (ThreadPoolExecutor) okHttpClient().dispatcher().executorService()));
        intending(not(isInternal())).respondWith(intentResult);

        if (mLaunchActivity) {
            mActivityLauncher.launchActivity();
        }
    }

    @Override
    public Statement apply(Statement base, Description description) {
        String className = description.getTestClass().getSimpleName();
        String methodName = description.getMethodName();
        ScreenshotExternalResource screenshotExternalResource = new ScreenshotExternalResource(className, methodName);

        return super.apply(screenshotExternalResource.apply(base, description), description);
    }

    public void launchActivity() {
        launchActivity(null);
    }

    public void launchActivity(Intent intent) {
        if (mLaunchActivity) {
            throw new RuntimeException("Activity should have already been launched");
        }

        if (intent != null) {
            mActivityLauncher.launchActivity(intent);
        } else {
            mActivityLauncher.launchActivity();
        }
    }

    public Activity getActivity() {
        return mActivityLauncher.getActivity();
    }

    @Override
    protected void after() {
        super.after();
        Intents.release();
        mActivityLauncher.finish();
    }

    private class ScreenshotExternalResource extends ExternalResource {

        private String mClassName;
        private final String mMethodName;

        public ScreenshotExternalResource(String className, String methodName) {
            mClassName = className;
            mMethodName = methodName;
        }

        @Override
        protected void after() {
        }
    }
}

