package com.example.play.fakeserver;


import android.support.test.InstrumentationRegistry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

/**
 * Can add mocked responses to the tests
 */
public class MockServer {

    private static final int DEFAULT_PORT = 9999;
    private static MockWebServer sMockWebServer;
    private static PathDispatcher sPathDispatcher;

    public static void setup() {
        if(sMockWebServer == null) {
            sMockWebServer = new MockWebServer();
            sPathDispatcher = new PathDispatcher();
            sMockWebServer.setDispatcher(sPathDispatcher);
            try {
                sMockWebServer.start(DEFAULT_PORT);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            sPathDispatcher.reset();
        }
    }

    public static void enqueueSuccessResponse(String path, String asset) {
        sPathDispatcher.putMockedResponse(path, getMockResponseForStringAsset(200, asset, -1));
    }

    private static MockResponse getMockResponseForStringAsset(int statusCode, String asset, int errorCode) {
        MockResponse response = new MockResponse();
        if (asset != null) {
            try {
                InputStream is = InstrumentationRegistry.getTargetContext().getResources().getAssets().open("responses/" + asset);
                StringBuilder buf = new StringBuilder();
                BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String str;
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
                String body;
                if (errorCode != -1) {
                    body = buf.toString().replace("${@error_code}", Integer.toString(errorCode));
                } else {
                    body = buf.toString();
                }
                response.setBody(body);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        response.setResponseCode(statusCode);
        return response;
    }
}
