package com.example.play.fakeserver;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

public class PathDispatcher extends okhttp3.mockwebserver.Dispatcher {

    private static final String TAG = PathDispatcher.class.getSimpleName();

    private Map<String, MockResponse> mResponseMap = new HashMap<>();
    private List<String> mDispatchedPaths = new ArrayList<>();
    private List<String> mDispathedUris = new ArrayList<>();

    @Override
    public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
        String requestPath = request.getPath();

        mDispathedUris.add(requestPath);
        if(requestPath.contains("?")) {
            requestPath = requestPath.substring(0, requestPath.indexOf('?'));
        }
        mDispatchedPaths.add(requestPath);
        MockResponse mockResponse = null;
        for(String keyPath : mResponseMap.keySet()) {
            if(Pattern.matches(wildcardToRegex(keyPath), requestPath)) {
                mockResponse = mResponseMap.get(keyPath);
                break;
            }
        }
        if(mockResponse == null) {
            mockResponse =  new MockResponse();
            mockResponse.setResponseCode(200);
        }
        return mockResponse;
    }

    public void reset() {
        mResponseMap.clear();
        mDispatchedPaths.clear();
        mDispathedUris.clear();
    }

    public void putMockedResponse(String path, MockResponse mockResponse) {
        mResponseMap.put(path, mockResponse);
    }

    public List<String> getRequestedPaths() {
        return mDispatchedPaths;
    }

    public List<String> getRequestedUris() {
        return mDispathedUris;
    }

    private static String wildcardToRegex(String wildcard){
        StringBuffer s = new StringBuffer(wildcard.length());
        s.append('^');
        for (int i = 0, is = wildcard.length(); i < is; i++) {
            char c = wildcard.charAt(i);
            switch(c) {
                case '*':
                    s.append(".*");
                    break;
                case '?':
                    s.append(".");
                    break;
                // escape special regexp-characters
                case '(': case ')': case '[': case ']': case '$':
                case '^': case '.': case '{': case '}': case '|':
                case '\\':
                    s.append("\\");
                    s.append(c);
                    break;
                default:
                    s.append(c);
                    break;
            }
        }
        s.append('$');
        return(s.toString());
    }
}
