package com.example.play.util;


import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;

public class ActivityLauncher<T extends Activity> {

    private static final String TAG = "ActivityLauncher";
    private final Class<T> mActivityClass;
    private final Instrumentation mInstrumentation;
    private T mActivity;

    public ActivityLauncher(Class<T> activityClass) {
        mActivityClass = activityClass;
        mInstrumentation = InstrumentationRegistry.getInstrumentation();
    }

    public T getActivity() {
        return mActivity;
    }

    public void launchActivity(@Nullable Intent startIntent) {
        mInstrumentation.setInTouchMode(false);
        String targetPackage = mInstrumentation.getTargetContext().getPackageName();
        if (startIntent == null) {
            startIntent = getDefaultIntent();
        }
        startIntent.setClassName(targetPackage, mActivityClass.getName());
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity = mActivityClass.cast(mInstrumentation.startActivitySync(startIntent));
        mInstrumentation.waitForIdleSync();
    }

    public void launchActivity() {
        launchActivity(getDefaultIntent());
    }

    public void finish() {
        if (mActivity != null) {
            mActivity.finish();
            mInstrumentation.waitForIdleSync();
            mActivity = null;
        }
    }

    private Intent getDefaultIntent() {
        return new Intent("android.intent.action.MAIN");
    }
}