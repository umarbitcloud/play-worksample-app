package com.example.play.util;

import android.support.test.espresso.IdlingResource;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Provides a way to monitor a Thread Pool Executor's work queue to ensure that there is no work pending
 * or executing (and to allow notification of idleness).
 * <p/>
 * Register a new subclass for each Thread Pool Executor.
 */
public class ThreadPoolIdlingResource implements IdlingResource {

    private static final String TAG = "ThreadPoolIdlingResource";

    private final ThreadPoolExecutor threadPoolExecutor;

    private ResourceCallback callback;
    private AtomicBoolean isMonitorForIdle = new AtomicBoolean(false);

    public ThreadPoolIdlingResource(ThreadPoolExecutor executor) {
        this.threadPoolExecutor = executor;
    }

    @Override
    public String getName() {
        return "OkHttpExecutorIdlingResource";
    }

    /**
     * Checks if the minPoolThreads is idle at this moment.
     *
     * @return true if the minPoolThreads is idle, false otherwise.
     */
    @Override
    public boolean isIdleNow() {
        // The minPoolThreads executor hasn't been injected yet, so we're idling
        boolean idle;
        if (!threadPoolExecutor.getQueue().isEmpty()) {
            idle = false;
        } else {
            int activeCount = threadPoolExecutor.getActiveCount();
            idle = 0 == activeCount;
        }
        if(!idle) {
            isMonitorForIdle.set(true);
        }
        if (idle && isMonitorForIdle.get()) {
            isMonitorForIdle.set(false);
            callback.onTransitionToIdle();
        }

        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(final ResourceCallback resourceCallback) {
        this.callback = resourceCallback;
    }
}