package com.example.play.repository;

import com.example.play.loader.PlayDetailLoader;
import com.example.play.loader.PlayLoader;
import com.example.play.util.ConnectivityChecker;

import org.jetbrains.annotations.NotNull;

import android.support.annotation.NonNull;

import javax.inject.Inject;

public class MainRepository {

    private final PlayLoader mPlayLoader;
    private final PlayDetailLoader mPlayDetailLoader;
    private final ConnectivityChecker mConnectivityChecker;

    @Inject
    public MainRepository(@NotNull PlayLoader playLoader, @NonNull PlayDetailLoader playDetailLoader,
            ConnectivityChecker connectivityChecker) {
        this.mPlayLoader = playLoader;
        this.mPlayDetailLoader = playDetailLoader;
        this.mConnectivityChecker = connectivityChecker;
    }

    public void getPlaySectionList() {
        if (mConnectivityChecker.isConnected()) {
            mPlayLoader.requestData(null);
        } else {
            mPlayLoader.getServerPlaySectionListFromDB();
        }
    }

    public void clearPlaySectionListLoader() {
        mPlayLoader.clearObserver();
    }

    public void getPlaySectionDetail(String playName, String playSectionId) {
        if (mConnectivityChecker.isConnected()) {
            mPlayDetailLoader.requestData(playName);
        } else {
            mPlayDetailLoader.getServerPlaySelectionFromDB(playSectionId);
        }
    }

    public void clearPlaySectionDetailLoader() {
        mPlayDetailLoader.clearObserver();
    }
}
