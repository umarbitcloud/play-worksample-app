package com.example.play.event.play;

import com.example.play.data.model.server.ServerPlaySelection;

import java.util.List;

/**
 * Event used when play request to server is successful
 */
public class PlaySuccessEvent {
    private List<ServerPlaySelection> mPlays;

    private PlaySuccessEvent(List<ServerPlaySelection> plays) {
        mPlays = plays;
    }

    public List<ServerPlaySelection> getPlays() {
        return mPlays;
    }

    public static PlaySuccessEvent playsEvent(List<ServerPlaySelection> plays) {
        return new PlaySuccessEvent(plays);
    }

}
