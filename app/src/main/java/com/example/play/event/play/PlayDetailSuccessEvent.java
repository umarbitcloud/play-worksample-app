package com.example.play.event.play;

import com.example.play.data.model.server.ServerPlayDetailResponse;

public class PlayDetailSuccessEvent {
    private ServerPlayDetailResponse mPlayDetail;

    private PlayDetailSuccessEvent(ServerPlayDetailResponse playDetail) {
        mPlayDetail = playDetail;
    }

    public ServerPlayDetailResponse getPlayDetail() {
        return mPlayDetail;
    }

    public static PlayDetailSuccessEvent playDetailEvent(ServerPlayDetailResponse playDetail) {
        return new PlayDetailSuccessEvent(playDetail);
    }

}
