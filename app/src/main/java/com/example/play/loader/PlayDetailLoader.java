package com.example.play.loader;

import com.example.play.data.model.server.ServerPlayDetailResponse;
import com.example.play.data.rest.PlayService;
import com.example.play.data.room.ServerPlaySelectionDAO;
import com.example.play.event.play.PlayDetailErrorEvent;
import com.example.play.event.play.PlayDetailSuccessEvent;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PlayDetailLoader implements CommonLoader<String> {

    private final EventBus mEventBus;
    private final PlayService mPlayService;
    private CompositeDisposable mCompositeDisposable;
    private final ServerPlaySelectionDAO mServerPlaySelectionDAO;

    @Inject
    public PlayDetailLoader(@NotNull EventBus eventBus, @NotNull PlayService playService, ServerPlaySelectionDAO serverPlaySelectionDAO,
            CompositeDisposable compositeDisposable) {
        this.mEventBus = eventBus;
        this.mPlayService = playService;
        this.mCompositeDisposable = compositeDisposable;
        this.mServerPlaySelectionDAO = serverPlaySelectionDAO;
    }

    @Override
    public void requestData(String data) {

        mCompositeDisposable.add(mPlayService.getPlayDetail(data).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<ServerPlayDetailResponse>() {
                    @Override
                    public void onSuccess(ServerPlayDetailResponse value) {
                        mServerPlaySelectionDAO.insertServerPlaySectionDetail(value);
                        mEventBus.post(PlayDetailSuccessEvent.playDetailEvent(value));
                    }

                    @Override
                    public void onError(Throwable e) {
                        mEventBus.post(new PlayDetailErrorEvent());
                    }
                }));

    }

    public void getServerPlaySelectionFromDB(String selectionId) {
        ServerPlayDetailResponse serverPlayDetailResponse = mServerPlaySelectionDAO.getServerPlaySelection(selectionId);
        if (serverPlayDetailResponse != null) {
            mEventBus.post(PlayDetailSuccessEvent.playDetailEvent(serverPlayDetailResponse));
        } else {
            mEventBus.post(new PlayDetailErrorEvent());
        }
    }

    @Override
    public void clearObserver() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
    }

}
