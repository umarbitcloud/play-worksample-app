package com.example.play.loader;

public interface CommonLoader<T> {
    void requestData(T data);
    void clearObserver();
}
