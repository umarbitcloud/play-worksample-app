package com.example.play.loader;

import com.example.play.data.model.server.ServerPlayResponse;
import com.example.play.data.model.server.ServerPlaySelection;
import com.example.play.data.rest.PlayService;
import com.example.play.data.room.ServerPlaySelectionDAO;
import com.example.play.event.play.PlayErrorEvent;
import com.example.play.event.play.PlaySuccessEvent;
import com.example.play.util.ConnectivityChecker;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PlayLoader implements CommonLoader<Void> {

    private final EventBus mEventBus;
    private final PlayService mPlayService;
    private CompositeDisposable mCompositeDisposable;
    private final ServerPlaySelectionDAO mServerPlaySelectionDAO;


    @Inject
    public PlayLoader(@NotNull EventBus eventBus, @NotNull PlayService playService,
            ServerPlaySelectionDAO serverPlaySelectionDAO, CompositeDisposable compositeDisposable) {
        this.mEventBus = eventBus;
        this.mPlayService = playService;
        this.mCompositeDisposable = compositeDisposable;
        this.mServerPlaySelectionDAO = serverPlaySelectionDAO;
    }

    @Override
    public void requestData(Void data) {

        mCompositeDisposable.add(mPlayService.getPlays().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<ServerPlayResponse>() {
                    @Override
                    public void onSuccess(ServerPlayResponse value) {
                        List<ServerPlaySelection> playSelectionList = value.getSectionPlayList().getPlaySections();
                        mServerPlaySelectionDAO.insertServerPlays(playSelectionList);
                        mEventBus.post(PlaySuccessEvent.playsEvent(playSelectionList));
                    }

                    @Override
                    public void onError(Throwable e) {
                        mEventBus.post(new PlayErrorEvent());
                    }
                }));

    }

    public void getServerPlaySectionListFromDB() {
        List<ServerPlaySelection> playSelectionList = mServerPlaySelectionDAO.getServerPlaySelection();
        if (playSelectionList != null && playSelectionList.size() > 0) {
            mEventBus.post(PlaySuccessEvent.playsEvent(playSelectionList));
        } else {
            mEventBus.post(new PlayErrorEvent());
        }

    }

    @Override
    public void clearObserver() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
    }

}
