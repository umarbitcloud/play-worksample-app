package com.example.play.base;

import com.example.play.di.component.ApplicationComponent;
import com.example.play.di.component.DaggerApplicationComponent;
import com.example.play.di.module.ApplicationModule;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;


public class BaseApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();

        component.inject(this);

        ApplicationModule.setApplication(this);

        return component;
    }

}
