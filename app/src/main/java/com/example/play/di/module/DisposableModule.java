package com.example.play.di.module;


import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Singleton
@Module
public class DisposableModule {

    @Provides
    public CompositeDisposable getCompositeDisposable() {
        return new CompositeDisposable();
    }
}
