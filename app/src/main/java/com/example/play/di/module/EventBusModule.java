package com.example.play.di.module;


import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class EventBusModule {
    private static EventBus sEventBus = EventBus.getDefault();

    @Provides
    public static EventBus eventBus() {
        return sEventBus;
    }
}
