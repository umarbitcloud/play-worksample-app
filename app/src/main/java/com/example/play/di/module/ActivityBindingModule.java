package com.example.play.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import com.example.play.ui.main.MainActivity;
import com.example.play.ui.main.MainFragmentBindingModule;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();
}
