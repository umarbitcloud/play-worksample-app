package com.example.play.di.module;

import com.example.play.data.room.ServerPlaySelectionDAO;
import com.example.play.data.room.ServerPlaySelectionDB;

import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.example.play.di.module.ApplicationModule.applicationContext;

@Singleton
@Module
public class DataBaseModule {

    @Provides
    public ServerPlaySelectionDB provideMyDatabase() {
        return Room.databaseBuilder(applicationContext(), ServerPlaySelectionDB.class, "play-db").allowMainThreadQueries().build();
    }

    @Provides
    public ServerPlaySelectionDAO provideServerPlaySelectionDao(ServerPlaySelectionDB myDatabase) {
        return myDatabase.mServerPlaySelectionDAO();
    }

}
