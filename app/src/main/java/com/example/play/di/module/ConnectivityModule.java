package com.example.play.di.module;

import com.example.play.util.ConnectivityChecker;
import com.example.play.util.NetworkConnectivityChecker;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.example.play.di.module.ApplicationModule.applicationContext;

@Singleton
@Module
public class ConnectivityModule {

    @Provides
    public static ConnectivityChecker networkConnectivityChecker() {
        return new NetworkConnectivityChecker(applicationContext());
    }
}
