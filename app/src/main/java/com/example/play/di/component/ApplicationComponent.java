package com.example.play.di.component;

import com.example.play.base.BaseApplication;
import com.example.play.di.module.ActivityBindingModule;
import com.example.play.di.module.ApplicationModule;
import com.example.play.di.module.ConnectivityModule;
import com.example.play.di.module.ContextModule;
import com.example.play.di.module.DataBaseModule;
import com.example.play.di.module.DisposableModule;
import com.example.play.di.module.EventBusModule;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {ContextModule.class, ApplicationModule.class, AndroidSupportInjectionModule.class, ActivityBindingModule.class,
        EventBusModule.class, ConnectivityModule.class, DataBaseModule.class, DisposableModule.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}