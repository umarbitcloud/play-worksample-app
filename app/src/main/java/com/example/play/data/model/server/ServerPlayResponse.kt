package com.example.play.data.model.server

import com.google.gson.annotations.SerializedName

data class ServerPlayResponse(

        @SerializedName("_links")
        val sectionPlayList: ServerPlaySectionList? = null

)
