package com.example.play.data.model.server

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ServerPlaySelection(
        @PrimaryKey
        @SerializedName("id")
        var id: String) {

    @Ignore
    constructor() : this("")

    @SerializedName("title")
    var title: String? = null

    @SerializedName("href")
    var link: String? = null

    @SerializedName("type")
    var type: String? = null
}
