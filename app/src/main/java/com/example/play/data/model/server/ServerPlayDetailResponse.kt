package com.example.play.data.model.server

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ServerPlayDetailResponse(@PrimaryKey
        @SerializedName("sectionId")
        var sectionId: String) {

    @Ignore
    constructor() : this("")

    @SerializedName("title")
    var selectionTitle: String? = null

    @SerializedName("description")
    var selectionDescription: String? = null

}
