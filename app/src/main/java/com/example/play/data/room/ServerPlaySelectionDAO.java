package com.example.play.data.room;

import com.example.play.data.model.server.ServerPlayDetailResponse;
import com.example.play.data.model.server.ServerPlaySelection;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ServerPlaySelectionDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertServerPlays(List<ServerPlaySelection> serverPlaySelectionList);

    @Query("SELECT * FROM ServerPlaySelection")
    List<ServerPlaySelection> getServerPlaySelection();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertServerPlaySectionDetail(ServerPlayDetailResponse serverPlayDetailResponse);

    @Query("SELECT * FROM ServerPlayDetailResponse WHERE sectionId LIKE :sectionId")
    ServerPlayDetailResponse getServerPlaySelection(String sectionId);

}
