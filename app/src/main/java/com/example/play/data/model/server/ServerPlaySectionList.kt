package com.example.play.data.model.server

import com.google.gson.annotations.SerializedName

data class ServerPlaySectionList (

    @SerializedName("viaplay:sections")
    val playSections: List<ServerPlaySelection>? = null
)
