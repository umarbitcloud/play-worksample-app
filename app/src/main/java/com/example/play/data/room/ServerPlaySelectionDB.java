package com.example.play.data.room;

import com.example.play.data.model.server.ServerPlayDetailResponse;
import com.example.play.data.model.server.ServerPlaySelection;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {ServerPlaySelection.class, ServerPlayDetailResponse.class}, version = 1, exportSchema = false)
public abstract class ServerPlaySelectionDB extends RoomDatabase {

    public abstract ServerPlaySelectionDAO mServerPlaySelectionDAO();

}
