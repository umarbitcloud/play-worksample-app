package com.example.play.data.rest;

import com.example.play.data.model.server.ServerPlayDetailResponse;
import com.example.play.data.model.server.ServerPlayResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface OpenPlayApi {

    @GET("androiddash-se")
    Single<ServerPlayResponse> getPlays();

    @GET("androiddash-se/{name}")
    Single<ServerPlayDetailResponse> getPlayDetail(@Path("name") String name);
}
