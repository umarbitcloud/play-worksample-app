package com.example.play.data.rest;

import com.example.play.data.model.server.ServerPlayDetailResponse;
import com.example.play.data.model.server.ServerPlayResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class PlayService {

    private final OpenPlayApi mOpenPlayApi;

    @Inject
    public PlayService(OpenPlayApi mOpenRepoApi) {
        this.mOpenPlayApi = mOpenRepoApi;
    }

    public Single<ServerPlayResponse> getPlays() {
        return mOpenPlayApi.getPlays();
    }

    public Single<ServerPlayDetailResponse> getPlayDetail(String name) {
        return mOpenPlayApi.getPlayDetail(name);
    }
}
