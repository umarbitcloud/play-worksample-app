package com.example.play.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Checks for network connectivity
 */
public class NetworkConnectivityChecker implements ConnectivityChecker {
    private Context mContext;
    public NetworkConnectivityChecker(Context context) {
        mContext = context;
    }

    @Override
    public boolean isConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
