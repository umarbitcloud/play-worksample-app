package com.example.play.util.string;

public class StringFormatterUtil {

    private static final String REG_ESP = ".*/([^/?{]+).*";

    public static String getLastBitFromUrl(final String url) {
        return url.replaceFirst(REG_ESP, "$1");
    }
}
