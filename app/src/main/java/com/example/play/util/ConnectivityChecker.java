package com.example.play.util;

/**
 * Interface to check for connection
 */
public interface ConnectivityChecker {
    boolean isConnected();
}
