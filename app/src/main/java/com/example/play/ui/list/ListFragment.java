package com.example.play.ui.list;

import com.example.play.R;
import com.example.play.base.BaseFragment;
import com.example.play.data.model.server.ServerPlaySelection;
import com.example.play.ui.detail.DetailsFragment;
import com.example.play.ui.detail.DetailsViewModel;
import com.example.play.util.ViewModelFactory;
import com.example.play.util.string.StringFormatterUtil;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;

public class ListFragment extends BaseFragment implements PlaySelectedListener {

    @BindView(R.id.play_selection_list)
    RecyclerView mListView;
    @BindView(R.id.tv_error_list)
    TextView mErrorTextView;
    @BindView(R.id.loading_view)
    View mLoadingView;

    @Inject
    ViewModelFactory mViewModelFactory;

    private ListViewModel mListViewModel;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_play_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.demo_play_list_action_title);
        mListViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ListViewModel.class);

        mListView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), DividerItemDecoration.VERTICAL));
        mListView.setAdapter(new PlayListAdapter(mListViewModel, this, this));
        mListView.setLayoutManager(new LinearLayoutManager(getContext()));

        observableViewModel();
    }

    @Override
    public void onPlaySelected(ServerPlaySelection playSection) {
        DetailsViewModel detailsViewModel = ViewModelProviders.of(getBaseActivity(), mViewModelFactory).get(DetailsViewModel.class);
        detailsViewModel.loadPlayDetail(StringFormatterUtil.getLastBitFromUrl(playSection.getLink()), playSection.getId());
        getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.screenContainer, new DetailsFragment())
                .addToBackStack(null).commit();
    }

    private void observableViewModel() {
        mListViewModel.getPlays().observe(this, plays -> {
            if (plays != null) {
                mListView.setVisibility(View.VISIBLE);
            }
        });

        mListViewModel.getError().observe(this, isError -> {
            if (isError != null) {
                if (isError) {
                    mErrorTextView.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.GONE);
                    mErrorTextView.setText(R.string.unknown_error);
                } else {
                    mErrorTextView.setVisibility(View.GONE);
                    mErrorTextView.setText(null);
                }
            }
        });

        mListViewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                mLoadingView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    mErrorTextView.setVisibility(View.GONE);
                    mListView.setVisibility(View.GONE);
                }
            }
        });
    }
}
