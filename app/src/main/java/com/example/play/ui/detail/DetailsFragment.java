package com.example.play.ui.detail;

import com.example.play.R;
import com.example.play.base.BaseFragment;
import com.example.play.util.ViewModelFactory;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;

public class DetailsFragment extends BaseFragment {


    @BindView(R.id.group)
    Group mPlayDetailsGroupView;
    @BindView(R.id.play_name)
    TextView mPlayTitleTextView;
    @BindView(R.id.play_description)
    TextView mPlayDescriptionTextView;
    @BindView(R.id.tv_error_detail)
    TextView mErrorTextView;
    @BindView(R.id.loading_detail_view)
    View mLoadingView;

    @Inject
    ViewModelFactory mViewModelFactory;
    private DetailsViewModel mDetailsViewModel;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_play_details;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.demo_play_detail_action_title);
        mDetailsViewModel = ViewModelProviders.of(getBaseActivity(), mViewModelFactory).get(DetailsViewModel.class);

        showPlayDetail();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void showPlayDetail() {

        mDetailsViewModel.getSelectedPlayDetail().observe(this, playDetail -> {
            if (playDetail != null) {
                mPlayDetailsGroupView.setVisibility(View.VISIBLE);
                mPlayTitleTextView.setText(playDetail.getSelectionTitle());
                mPlayDescriptionTextView.setText(playDetail.getSelectionDescription());
            }
        });

        mDetailsViewModel.getPlayDetailError().observe(this, isErrorDetail -> {
            if (isErrorDetail != null) {
                if (isErrorDetail) {
                    mErrorTextView.setVisibility(View.VISIBLE);
                    mPlayDetailsGroupView.setVisibility(View.GONE);
                    mErrorTextView.setText(R.string.unknown_error);
                } else {
                    mErrorTextView.setVisibility(View.GONE);
                    mErrorTextView.setText(null);
                }
            }
        });

        mDetailsViewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                mLoadingView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    mErrorTextView.setVisibility(View.GONE);
                    mPlayDetailsGroupView.setVisibility(View.GONE);
                }
            }
        });
    }
}
