package com.example.play.ui.main;

import com.example.play.ui.list.ListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import com.example.play.ui.detail.DetailsFragment;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract ListFragment provideListFragment();

    @ContributesAndroidInjector
    abstract DetailsFragment provideDetailsFragment();
}
