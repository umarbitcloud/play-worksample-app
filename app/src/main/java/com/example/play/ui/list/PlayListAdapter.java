package com.example.play.ui.list;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.play.R;
import com.example.play.data.model.server.ServerPlaySelection;

public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.RepoViewHolder>{

    private PlaySelectedListener mPlaySelectedListener;
    private final List<ServerPlaySelection> data = new ArrayList<>();

    PlayListAdapter(ListViewModel viewModel, LifecycleOwner lifecycleOwner, PlaySelectedListener playSelectedListener) {
        this.mPlaySelectedListener = playSelectedListener;
        viewModel.getPlays().observe(lifecycleOwner, plays -> {
            data.clear();
            if (plays != null) {
                data.addAll(plays);
                notifyDataSetChanged();
            }
        });
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_play_item, parent, false);
        return new RepoViewHolder(view, mPlaySelectedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static final class RepoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.play_title) TextView mPlayTitleView;
        @BindView(R.id.play_type) TextView mPlayTypeView;

        ServerPlaySelection mPlaySection;

        RepoViewHolder(View itemView, PlaySelectedListener playSelectedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(mPlaySection != null) {
                    playSelectedListener.onPlaySelected(mPlaySection);
                }
            });
        }

        void bind(ServerPlaySelection play) {
            this.mPlaySection = play;
            mPlayTitleView.setText(mPlaySection.getTitle());
            mPlayTypeView.setText(mPlaySection.getType());
        }
    }
}
