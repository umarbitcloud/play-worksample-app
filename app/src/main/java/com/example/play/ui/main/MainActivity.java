package com.example.play.ui.main;

import com.example.play.R;
import com.example.play.base.BaseActivity;
import com.example.play.ui.list.ListFragment;

import android.os.Bundle;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.screenContainer, new ListFragment()).commit();
    }
}
