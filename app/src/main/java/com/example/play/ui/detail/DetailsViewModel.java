package com.example.play.ui.detail;

import com.example.play.data.model.server.ServerPlayDetailResponse;
import com.example.play.event.play.PlayDetailErrorEvent;
import com.example.play.event.play.PlayDetailSuccessEvent;
import com.example.play.repository.MainRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

public class DetailsViewModel extends ViewModel {

    private final MainRepository mMainRepository;
    private EventBus mEventBus;

    private final MutableLiveData<ServerPlayDetailResponse> selectedPlayDetail = new MutableLiveData<>();
    private final MutableLiveData<Boolean> playDetailLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();


    public LiveData<ServerPlayDetailResponse> getSelectedPlayDetail() {
        return selectedPlayDetail;
    }

    public LiveData<Boolean> getPlayDetailError() {
        return playDetailLoadError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    @Inject
    public DetailsViewModel(MainRepository mainRepository, EventBus eventBus) {
        this.mMainRepository = mainRepository;
        this.mEventBus = eventBus;
        mEventBus.register(this);
    }


    public void loadPlayDetail(String playName, String playSectionId) {
        loading.setValue(true);
        mMainRepository.getPlaySectionDetail(playName, playSectionId);
    }

    @Subscribe
    public void onEvent(PlayDetailSuccessEvent event) {
        playDetailLoadError.setValue(false);
        loading.setValue(false);
        selectedPlayDetail.setValue(event.getPlayDetail());
    }

    @Subscribe
    public void onEvent(PlayDetailErrorEvent event) {
        playDetailLoadError.setValue(true);
        loading.setValue(false);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mEventBus.unregister(this);
        mMainRepository.clearPlaySectionDetailLoader();
    }
}
