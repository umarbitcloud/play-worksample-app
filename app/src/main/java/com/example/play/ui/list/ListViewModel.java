package com.example.play.ui.list;

import com.example.play.data.model.server.ServerPlaySelection;
import com.example.play.event.play.PlayErrorEvent;
import com.example.play.event.play.PlaySuccessEvent;
import com.example.play.repository.MainRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

public class ListViewModel extends ViewModel {

    private final MainRepository mMainRepository;

    private final MutableLiveData<List<ServerPlaySelection>> plays = new MutableLiveData<>();
    private final MutableLiveData<Boolean> playLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    private EventBus mEventBus;

    @Inject
    public ListViewModel(MainRepository mainRepository, EventBus eventBus) {
        this.mMainRepository = mainRepository;
        mEventBus = eventBus;
        mEventBus.register(this);
        fetchPlays();
    }

    public LiveData<List<ServerPlaySelection>> getPlays() {
        return plays;
    }

    public LiveData<Boolean> getError() {
        return playLoadError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    private void fetchPlays() {
        loading.setValue(true);
        mMainRepository.getPlaySectionList();
    }

    @Subscribe
    public void onEvent(PlaySuccessEvent event) {
        playLoadError.setValue(false);
        plays.setValue(event.getPlays());
        loading.setValue(false);
    }

    @Subscribe
    public void onEvent(PlayErrorEvent event) {
        playLoadError.setValue(true);
        loading.setValue(false);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mMainRepository.clearPlaySectionListLoader();
        mEventBus.unregister(this);
    }
}
