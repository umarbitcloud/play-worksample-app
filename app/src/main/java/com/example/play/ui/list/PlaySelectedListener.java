package com.example.play.ui.list;

import com.example.play.data.model.server.ServerPlaySelection;

public interface PlaySelectedListener {

    void onPlaySelected(ServerPlaySelection playSection);
}
