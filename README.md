ViaPlay Sample Android App
=====================

### Design methodology

* Summary : Clean abstraction of app components: All application components including activities, layouts, modules are defined in separated files and component dependency is reduced by use of event bus and dependency injection framework. Common application APIs and utilities are designed as dependency modules and could be injected in any class without any restriction.

* Framework : I have used MVVM design pattern for the app. The app works in both online and offline mode.

* Networking : Have used Rjava2 and Retrofit2 for the networking purpose.

* In-app communication: GreenRobot event bus is used as a comunnication protocol in the app.

* Disk saving:  Have used RoomDB for persisting the response data for the offline mode.

* Testing : Play sample App is run against unit test (Robolectric) and UI (Espresso) tests.

* Library's used : Well-known and well-tested libraries like Dagger2, OkHttp3, Retrofit2, Gson, ButterKnife, Rxjava2, RoomDB, Lifecycle aware (livedata),  Junit, Robolectric, Assetj, Mockito, Espresso.

* Kotlin Capability :  Have created Kotlin data classes for consuming the model objects along with dao's.  

### Nice to have items (Improvements)

* Richer UI design

* Extend more with kotlin based development

* Need to add more ui and unit test cases

* Better error handlings

### Build/Repo details

* If required, I can share my private repo details.

### Run Espresso tests

* Use the below gradle commands to execute the ui and unit test

        ./gradlew clean connectedDebugAndroidTest

#### Run unit tests

      ./gradlew clean testDebugUnitTest

